#### Security Group for ALB
resource "aws_security_group" "alb_sg" {
   name        = "alb-sg"
   description = "Allow inbound traffic and outbound traffic"
   vpc_id      = var.vpc_id
   tags        = merge(local.tags, map("Name", "alb-sg"))
 }
resource "aws_security_group_rule" "ingress_alb_sg" {
   type                     = "ingress"
   protocol                 = "-1"
   from_port                = 0
   to_port                  = 0
   security_group_id        = aws_security_group.alb_sg.id
   source_security_group_id = var.alb_sg_id
 }
resource "aws_security_group_rule" "ingress_alb_sg1" {
   type              = "ingress"
   protocol          = "-1"
   from_port         = "0"
   to_port           = "0"
   security_group_id = aws_security_group.alb_sg.id
   self              = true
 }

resource "aws_security_group_rule" "egress_alb_sg" {
   type              = "egress"
   from_port         = 0
   to_port           = 0
   protocol          = "-1"
   cidr_blocks       = ["0.0.0.0/0"]
   security_group_id = aws_security_group.alb_sg.id
 }

resource "aws_security_group_rule" "ingress_rules" {
  count = length(var.ingress_rules)

  type              = "ingress"
  from_port         = var.ingress_rules[count.index].from_port
  to_port           = var.ingress_rules[count.index].to_port
  protocol          = var.ingress_rules[count.index].protocol
  cidr_blocks       = [var.ingress_rules[count.index].cidr_block]
  security_group_id = aws_security_group.alb_sg.id
}
