  locals {
    tags = {
      Team                  = var.team
      AppEnvironment        = var.env
      AppName               = var.app
      ITOwnerEmail          = "ygvp"
      BillingCode           = "XXXXXXXX"
      DataClassification    = "Highly Restricted"
      TaggingVersion        = "0.0.1"
    }
  }
