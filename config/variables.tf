variable "region" {
    default = " "
  }
  variable "vpc_id" {
    default = " "
  }
  variable "subnet_ids" {
    description = "EC2 subnets"
    type        = list
  }
  
  variable "account_id" {
    default = ""
    description = "AWS account id"
  }
  
  variable "env" {
    default = " "
  }
  variable "app" {
    default = ""
  }
  variable "team" {
    default = ""
  }
  variable "domain_name" {
    default = ""
  }
variable "alb_sg_id" {
    default = ""
  }

variable "ingress_rules" {
    type = list(object({
      from_port   = number
      to_port     = number
      protocol    = string
      cidr_block  = string
    }))
}
