region                          = "us-east-1"
vpc_id                          = "vpc-0b3fba03fb0410990"
subnet_ids                      = ["subnet-050cc84ce294bb785", "subnet-0a6d6880f0f123051", "subnet-01f2bbf318d8bc150"]  # public subnets
team                            = "tgrc-devops"
domain_name                     = "tgrc.gmail.com"

app                             = "alb"
env                             = "dev"
account_id                      = 161829588545
alb_sg_id                       = "sg-0e6bbf9b8a6db2bb6"
ingress_rules                   = [
        {
          from_port   = 22
          to_port     = 22
          protocol    = "tcp"
          cidr_block  = "1.2.3.4/32"
        },
        {
          from_port   = 23
          to_port     = 23
          protocol    = "tcp"
          cidr_block  = "1.2.3.4/32"
        },
    ]
